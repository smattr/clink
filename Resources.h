#pragma once

#include "AsmParser.h"
#include "CXXParser.h"
#include "Symbol.h"
#include "WorkQueueStub.h"

/* Management of C/C++ and assembly parsers that we'll lazily construct. It's
 * possible we'll not need one or both of them, in which case we don't have to
 * pay the construction overhead.
 *
 * Note that the implicit expectation is that these resources are thread-local.
 * The class assumes it does not have to do any mutual exclusion or
 * synchronisation.
 */
class Resources {

 public:
  Resources(SymbolConsumer *consumer, WorkQueue *wq) noexcept
    : consumer(consumer), wq(wq) {
  }

  // It doesn't make sense to copy one of these.
  Resources(const Resources &) = delete;

  Resources(Resources &&other) noexcept {
    CXXParser *cxx_parser = other.cxx_parser;
    AsmParser *asm_parser = other.asm_parser;
    SymbolConsumer *consumer = other.consumer;
    WorkQueue *wq = other.wq;
    other.cxx_parser = nullptr;
    other.asm_parser = nullptr;
    other.consumer = nullptr;
    other.wq = nullptr;
    this->cxx_parser = cxx_parser;
    this->asm_parser = asm_parser;
    this->consumer = consumer;
    this->wq = wq;
  }

  ~Resources() {
    delete cxx_parser;
    delete asm_parser;
  }

  // As above, it doesn't make sense to copy one of these.
  Resources &operator=(const Resources &) = delete;

  Resources &operator=(Resources &&other) noexcept {
    CXXParser *cxx_parser = other.cxx_parser;
    AsmParser *asm_parser = other.asm_parser;
    SymbolConsumer *consumer = other.consumer;
    WorkQueue *wq = other.wq;
    other.cxx_parser = nullptr;
    other.asm_parser = nullptr;
    other.consumer = nullptr;
    other.wq = nullptr;
    delete this->cxx_parser;
    delete this->asm_parser;
    this->cxx_parser = cxx_parser;
    this->asm_parser = asm_parser;
    this->consumer = consumer;
    this->wq = wq;
    return *this;
  }

  CXXParser *get_cxx_parser() {
    if (cxx_parser == nullptr)
      cxx_parser = new CXXParser;
    return cxx_parser;
  }

  AsmParser *get_asm_parser() {
    if (asm_parser == nullptr)
      asm_parser = new AsmParser;
    return asm_parser;
  }

  SymbolConsumer *consumer;
  WorkQueue *wq;

 private:
  CXXParser *cxx_parser = nullptr;
  AsmParser *asm_parser = nullptr;

};
