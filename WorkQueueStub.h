#pragma once

/* Nothing but a forward declaration. Include this if you need to deal with
 * WorkQueue pointers, but can't include WorkQueue.h without creating a circular
 * include.
 */
class WorkQueue;
